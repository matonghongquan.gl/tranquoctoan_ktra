﻿using System.ComponentModel.DataAnnotations;

namespace KiemTraLTWeb.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public string? CustomerFirstname { get; set; }
        public string? CustomerLastname { get; set; }
       
        public string? CustomerContact { get; set; }
        public string? CustomerAddress { get; set; }
        public string? CustomerUsername { get; set; }
        public string? CustomerPassword { get; set; }

        public ICollection<Account>? Accounts { get; set; }
        public ICollection<Transaction>? Transactions { get; set; }
    }
}
