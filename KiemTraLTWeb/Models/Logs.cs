﻿using System.ComponentModel.DataAnnotations;

namespace KiemTraLTWeb.Models
{
    public class Logs
    {
        [Key]
        public int LogsId { get; set; }
        public int TransactionId { get; set; }
        public DateTime? LoginDate { get; set; }
        public DateTime? LoginTime { get; set; }

        public ICollection<Report>? Report { get; set; }
    }
}
