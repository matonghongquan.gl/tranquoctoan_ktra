﻿using System.ComponentModel.DataAnnotations;

namespace KiemTraLTWeb.Models
{
    public class Transaction
    {
        [Key]
        public int TransactionId { get; set; }
       
        public string? TransactionName { get; set; }

        public ICollection<Logs>? Logs { get; set; }


        public ICollection<Report>? Re { get; set; }
    }
}

