﻿using System.ComponentModel.DataAnnotations;

namespace KiemTraLTWeb.Models
{
    public class Report
    {
        [Key]
        public int ReportId { get; set; }     
        public string? ReportName { get; set; }
        public DateTime ReportDate { get; set; }

       
    }
}
