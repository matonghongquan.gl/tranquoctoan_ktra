﻿using System.ComponentModel.DataAnnotations;

namespace KiemTraLTWeb.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        public string? EmployeeFirstname { get; set; }
        public string? EmployeeLastname { get; set; }
        public string? EmployeeContact { get; set; }
        public string? EmployeeAddress { get; set; }
        public string? EmployeeUsername { get; set; }
        public string? EmployeePassword { get; set; }

        public ICollection<Transaction>? Transactions { get; set; }
    }
}
