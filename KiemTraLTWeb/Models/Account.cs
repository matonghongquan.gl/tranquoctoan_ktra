﻿using System.ComponentModel.DataAnnotations;

namespace KiemTraLTWeb.Models
{
    public class Account
    {
        [Key]
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }
        public string? AccountName { get; set; }
        public ICollection<Report>? Reports { get; set; }
        
        
    }
}
